package com.ubo.insta.storyapi.shared.users;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import java.util.List;
import swagger.userapi.Right;

public interface UserClient {

  @RequestLine("GET /users/{userId}/rights")
  @Headers({"Authentication: {userId}"})
  List<Right> getRights(@Param("userId") String userId);

  @RequestLine("GET /check")
  void check();

}
