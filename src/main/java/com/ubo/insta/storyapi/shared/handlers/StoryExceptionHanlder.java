package com.ubo.insta.storyapi.shared.handlers;

import com.ubo.insta.storyapi.shared.exceptions.StoryException;
import java.util.List;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import swagger.storyapi.Error;

public class StoryExceptionHanlder implements ExceptionMapper<StoryException> {

 @Override
 public Response toResponse(StoryException exception) {
  var error = new Error();
  error.setCode(String.valueOf(exception.getCode()));
  error.setContent(List.of(exception.getMessage()));
  return Response.status(Status.BAD_REQUEST).entity(error).build();
 }
}
