package com.ubo.insta.storyapi.shared.users;

public record RightEntity(String id, String label, boolean isAvtive) {

}
