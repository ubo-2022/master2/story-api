package com.ubo.insta.storyapi.shared.users;

import swagger.userapi.Right;

public class RightsMapper {

 public static RightEntity map(Right right) {
  return new RightEntity(right.getId(), right.getRightName(), right.isIsActive());
 }

 public static Right map(RightEntity rightEntity) {
  Right right = new Right();
  right.setRightName(rightEntity.label());
  right.setId(rightEntity.id());
  right.setIsActive(rightEntity.isAvtive());
  return right;
 }

}
