package com.ubo.insta.storyapi.shared.exceptions;

public class StoryException extends RuntimeException {
 int code;
 public StoryException (int code, String message) {
  super(message);
  this.code = code;
 }

 public int getCode() {
  return this.code;
 }
}
