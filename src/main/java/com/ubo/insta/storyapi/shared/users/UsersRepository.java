package com.ubo.insta.storyapi.shared.users;

import java.util.List;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import swagger.userapi.Right;

@Component
public class UsersRepository {

 @Autowired
 private UserClient userClient;

 public List<Right> getUserRights(String userId) {
  return this.userClient.getRights(userId);
 }
}
