package com.ubo.insta.storyapi.configuration;

import com.ubo.insta.storyapi.check.CheckController;
import com.ubo.insta.storyapi.shared.filters.AuthenticationRequiredImpl;
import com.ubo.insta.storyapi.shared.handlers.StoryExceptionHanlder;
import com.ubo.insta.storyapi.story.StoryController;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
@ApplicationPath("/api/v1")
public class JerseyConfig  extends ResourceConfig {

  public JerseyConfig() {
    register(CheckController.class);
    register(StoryController.class);
    register(AuthenticationRequiredImpl.class);
    register(StoryExceptionHanlder.class);
  }
}
