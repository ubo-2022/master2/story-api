package com.ubo.insta.storyapi.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ubo.insta.storyapi.shared.users.UserClient;
import feign.Feign;
import feign.Logger;
import feign.Logger.JavaLogger;
import feign.Logger.Level;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.ConnectionPool;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients
public class FeignConfig {

 @Inject
 private ObjectMapper objectMapper;

 @Bean("userClient")
 public UserClient getUserClient() {
  return Feign.builder()
    .encoder(new JacksonEncoder(objectMapper))
    .decoder(new JacksonDecoder(objectMapper))
    .logger(new JavaLogger(FeignConfig.class))
    .logLevel(Level.FULL)
    .client(new OkHttpClient())
    .target(UserClient.class, "http://localhost:8090/api/v1");
 }

 private okhttp3.OkHttpClient getOkHttpClient() {
  var okHttpClient = new okhttp3.OkHttpClient.Builder();
  okHttpClient.readTimeout(10000, TimeUnit.MILLISECONDS);
  okHttpClient.connectTimeout(1000, TimeUnit.MILLISECONDS);
  okHttpClient
    .connectionPool(new ConnectionPool(5, 5, TimeUnit.MINUTES));
  return okHttpClient.build();
 }
}
