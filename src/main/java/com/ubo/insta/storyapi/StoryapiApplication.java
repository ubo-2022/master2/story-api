package com.ubo.insta.storyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoryapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoryapiApplication.class, args);
	}

}
