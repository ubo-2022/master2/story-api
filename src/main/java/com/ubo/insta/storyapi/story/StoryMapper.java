package com.ubo.insta.storyapi.story;

import com.ubo.insta.storyapi.story.entity.StoryEntity;
import java.math.BigDecimal;
import java.util.Optional;
import swagger.storyapi.Story;

public class StoryMapper {

  public static StoryEntity map(Story input) {
    StoryEntity output = new StoryEntity();
    output.setContent(input.getContent());
    output.setId(input.getId());
    output.setName(input.getName());
    //output.setDate(input.getDate().doubleValue());
    return output;
  }

  public static Story map(StoryEntity storyEntity) {
    Story story = new Story();
    story.setContent(storyEntity.getContent());
    story.setId(storyEntity.getId());
    story.setName(storyEntity.getName());
    if (storyEntity.getDate() != null) {
      story.setDate(BigDecimal.valueOf(storyEntity.getDate().toEpochMilli()));
    }
    return story;
  }

}
