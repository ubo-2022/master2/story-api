package com.ubo.insta.storyapi.story.entity;

import java.time.Instant;
import lombok.Data;

@Data
public class StoryEntity {

 private String id;
 private String name;
 private String content;
 private Instant date;

}
