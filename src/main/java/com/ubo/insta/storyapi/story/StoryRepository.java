package com.ubo.insta.storyapi.story;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import swagger.storyapi.Story;

@Component
public class StoryRepository {

  private static final String CREATE_STORY_SQL = "INSERT INTO STORY VALUES (:id, :name, :content, SYSDATE)";

  private static final String INCREMENT_SEQ_SQL = "SELECT STORY_SEQ.NEXTVAL FROM DUAL";
  @Inject
  private NamedParameterJdbcTemplate instaTemplate;

  public void saveStory(Story story) {
    Map<String, Object> params = new HashMap<>();
    params.put("id", getStoryId());
    params.put("name", story.getName());
    params.put("content", story.getContent());
    this.instaTemplate.update(CREATE_STORY_SQL, params);
  }

  private Integer getStoryId() {
    return this.instaTemplate.queryForObject(INCREMENT_SEQ_SQL, new HashMap<>(), Integer.class);
  }


}
