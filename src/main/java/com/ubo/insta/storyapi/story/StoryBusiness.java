package com.ubo.insta.storyapi.story;

import static com.ubo.insta.storyapi.story.StoryMapper.map;

import com.ubo.insta.storyapi.shared.exceptions.StoryException;
import com.ubo.insta.storyapi.shared.users.RightsMapper;
import com.ubo.insta.storyapi.shared.users.UsersRepository;
import com.ubo.insta.storyapi.story.entity.StoryEntity;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Component;

@Component
public class StoryBusiness {

 @Inject
 private UsersRepository usersRepository;
 @Inject
 private StoryRepository storyRepository;
 public void createStory(StoryEntity storyEntity, String userId) {
  if (storyEntity.getId() != null) {
   throw new StoryException(100, "Le client ne doit pas fournir un id story.");
  }
  var rights = this.usersRepository.getUserRights(userId)
    .stream()
    .map(RightsMapper::map).toList();
  rights.forEach(right -> {
   if (right.label().equals("CREATE_STORY")) {
    if (!right.isAvtive()) {
     throw new StoryException(403, "Le client n'est pas authorisé à créer des stories");
    }
   }
  });
  this.storyRepository.saveStory(map(storyEntity));
 }
}
