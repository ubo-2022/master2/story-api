package com.ubo.insta.storyapi.story;

import static com.ubo.insta.storyapi.story.StoryMapper.map;

import com.ubo.insta.storyapi.shared.filters.AuthenticationRequired;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import swagger.storyapi.Story;

@Path("/story/{userId}")
public class StoryController {

  @Inject
  private StoryBusiness storyBusiness;

 @POST
 @Produces(MediaType.APPLICATION_JSON)
 @AuthenticationRequired
 public Response createStory(@PathParam("userId") String userId,  Story story) {
   storyBusiness.createStory(map(story), userId);
   return Response.ok().build();
 }
}
